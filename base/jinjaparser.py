#!/usr/bin/env python3

import os

from jinja2 import Environment, PackageLoader, select_autoescape

JINJA_FILE = os.path.join(os.path.dirname(__file__), 'nginx.jinja')


def main():
    env = Environment()
    env.globals['exists'] = os.path.exists
    env.globals['Env'] = os.environ
    with open(JINJA_FILE, 'r') as f:
        s = f.read()
        template = env.from_string(s)

    hostnames = ['sicily-demo.iuvia.io', 'sicily-demo.demo.iuvia.io']
    avahi_hostname = 'sicily-demo-iuvia-io.local'

    print(template.render(
        tlds=hostnames,
        avahi_hostname=avahi_hostname,
        services=[
            {
                'name': 'boxui',
                'domain': 'sicily-demo.iuvia.io',
                'path': '/boxui/',
                'default': 0,
            'backend': 'unix:/run/services/http/boxui.sock',
            }
        ]
    ))


if __name__ == '__main__':
    main()

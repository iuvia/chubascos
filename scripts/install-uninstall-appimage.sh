#!/bin/bash

API_HOST=http://localhost:8001


install_appimage () {
	local codename=$1
	local version=$2
	curl -s "$API_HOST/appimages/install?codename=${codename}&version=${version}" -H 'User-Agent: Testbench/0.1' -H 'Accept: application/json' -H 'Cache-Control: no-cache' --data-raw ''
}

uninstall_appimage () {
	local codename=$1
	local version=$2
	local instance=$3
	
	curl -s "$API_HOST/appimages/uninstall?codename=${codename}&version=${version}&instance=${instance}" -H 'User-Agent: Testbench/0.1' -H 'Accept: application/json' -H 'Cache-Control: no-cache' --data-raw ''
}

installed_appimages () {
	curl -s "$API_HOST/appimages/installed" -H 'User-Agent: Testbench/0.1' -H 'Accept: application/json' -H 'Cache-Control: no-cache'
}

known_appimages () {
	curl -s "$API_HOST/appimages" -H 'User-Agent: Testbench/0.1' -H 'Accept: application/json' -H 'Cache-Control: no-cache'
}

if [[ "$#" -lt 2 ]]; then
	echo "Testing with helloworld.. $1 $2 $3 $4"

	codename=${1:-io.iuvia.demo.helloworld}
	version=${2:-0.0.1}
	
	r1=$(install_appimage $codename $version)
	echo "GOT: $r1"
	instance=$(echo "$r1"|jq -r .instance)

	sleep 5
	
	r2=$(uninstall_appimage $codename $version $instance)
	echo "GOT: $r2"
	status=$(echo "$r2" | jq -r .status)
	[[ "$status" == "ok" ]]
	exit $?
else
	$1 $2 $3 $4 $5
fi

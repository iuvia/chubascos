FROM debian:10

ADD ./bootstrap.sh /tmp/bootstrap.sh

RUN /tmp/bootstrap.sh

LABEL maintainer=team@heimdall.cloud
LABEL version=0.1.0

